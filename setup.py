from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='A example of using cookiecutter as data science project management tool',
    author='Weitingting Liu',
    license='',
)
